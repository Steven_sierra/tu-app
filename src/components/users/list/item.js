import React from 'react'
import { ListItem,
         Icon,
         Grid,
         Typography } from '@material-ui/core'

const Item = ({user: { first, last, email}, viewUser}) =>{
    return<ListItem 
            style={{padding: '15px', backgroundColor: 'white'}} 
            onClick={viewUser}
            button 
            divider 
        >
        <Grid container justify='space-between'>
            <div>
                <Typography variant='subheading'>{`${first} ${last}`}</Typography>
                <Typography color='textSecondary'>{email}</Typography>
            </div>
            <div>
                <Icon color='disabled'>star</Icon>
            </div>
        </Grid>
    </ListItem>
}

export default Item