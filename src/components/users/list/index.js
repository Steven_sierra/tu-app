import React from 'react'
import { RingLoader } from 'react-spinners'
import ListMaterial from '@material-ui/core/List';

import Item from './item'

const List = ({users, viewUser}) => {
    if(users){
        return <ListMaterial  component="nav">
            {users.map((user, index) => <Item 
                                            key={index} 
                                            user={user}
                                            viewUser={viewUser} 
                                        />)}
        </ListMaterial >
    }else{
        return<RingLoader />
    }
}

export default List;