import React from 'react'
import { Avatar, 
         Grid,
         Icon,
         Typography } from '@material-ui/core';

const Profile = ({user, close}) => {
    const photo = user.picture.thumbnail
    const { phone, location: {street} } = user
    const name = `${user.name.first} ${user.name.last}`
    const fullname =name.charAt(0).toUpperCase() + name.slice(1)

    return <div>
            <div style={{padding: '20px 20px 5px ', backgroundColor: '#36D7B7'}}>
                <Grid container direction='row'>
                    <Avatar 
                        alt="Remy Sharp" 
                        src={photo} 
                        style={{width: 50,
                                height: 50}}
                    />
                    <div style={{marginLeft: 10, marginBottom: 5}}>
                        <Typography variant='title'>{fullname}</Typography>
                        <Typography variant='body2' color='textSecondary'>
                            <Icon 
                                fontSize='inherit'
                                style={{marginRight: 5, marginTop: 5}}
                            >
                                phone
                            </Icon>
                            {phone}
                        </Typography>
                    </div>
                </Grid>
            </div>
            <Grid container justify='center' style={{padding: '10px', backgroundColor: '#2ABB9B', color: 'white'}}>
                <Icon 
                    fontSize='inherit'
                    style={{marginRight: 5}}
                >
                    room
                </Icon>
                <Typography style={{color:'white'}}>{street}</Typography>
            </Grid>
    </div>
}

export default Profile