import React, { Component } from 'react'
import { Dialog } from '@material-ui/core';
import { AppContext } from '../../App' 

import List from './list'
import Profile from './profile'

class Users extends Component{

    render(){
        return <AppContext.Consumer>
                {({ users,
                    open,
                    close,
                    viewUser,
                    user }) => (
                    <div style={{marginTop: '20px'}}>
                        <List users={users} viewUser={viewUser} />
                        <Dialog open={open} onClose={close}>
                            <Profile close={close} user={user}/>
                        </Dialog>
                    </div>
                )}
            </AppContext.Consumer>
    }
}

export default Users