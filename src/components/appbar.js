import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Grid, IconButton } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

function ButtonAppBar() {
  return (
    <div>
      <AppBar position="static">
        <Grid container direction='row' alignItems='center'>
            <Toolbar>
            <IconButton color="inherit" aria-label="Menu">
                <MenuIcon />
            </IconButton>
            </Toolbar>
            <Typography style={{color:'white'}} variant='title'>Tu Aplicacion</Typography>
        </Grid>
      </AppBar>
    </div>
  );
}

export default ButtonAppBar