const getUserList = async() => {
    const users = await fetch('https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb')
    const response = await users.json()
    return response.results[0]
}

const getUser = async() => {
    const user = await fetch('https://randomuser.me/api/')
    const response = await user.json()
    return response.results[0]
}

export { getUserList, getUser }