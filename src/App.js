import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';

import './App.css';
import Users from './components/users'
import ButtonAppBar from './components/appbar'
import { getUserList,
         getUser } from './request';

const AppContext = React.createContext();

class App extends Component {

  state = { users: [],
            open: false,
            user: {} }

  async componentDidMount(){
    const users = await getUserList()
    if(users) this.setState({users})
  }

  viewUser = async () => {
    const user = await getUser()
    if(user) this.setState({ user, open:true })
  }

  toggleDialog = () => this.setState(prevState => { 
    return { open: !prevState.open }
   })

  render() {
    const state = this.state
    return (
      <AppContext.Provider
          value={{ ...state, viewUser: this.viewUser, close: this.toggleDialog }}
      >
        <ButtonAppBar />
        <Grid container justify='center'>
          <Users />
        </Grid>
      </AppContext.Provider>
    );
  }
}

export { AppContext, App };